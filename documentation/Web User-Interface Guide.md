# Web User-interface Guide

### Login

At login step user needs to enter user name as follows.

![](images/loginview.png)

After user name login user needs to select to which chat-room user wants to enter. This is done by selecting room from drop-down menu as follows.

![](images/selectroom.png)

### Chat

After entering the chat-room we can see all participants in the room.

![](images/roomview.png)

Now we can start chat by writing into message field as follows. You may send message either with enter or by pressing ”SEND”-button.

![](images/sendmessage.png)

After greeting chat-bot replies as follows.

![](images/chatview.png)

You may leave room by pressing ”LOGOUT” on the top left-hand corner

![](images/logout.png)

### Admin

You can enter into admin features by giving ”Admin” as user name in login view.

![](images/adminlogin.png)

After entering chat-room you have possibility to enter into admin features view by pressing ”ADMIN” on top view.

![](images/adminlink.png)

In admin view user can create new rooms and select bot type (Normal/Troll) for created room.

![](images/createroom.png)

When ”CREATE”-button is pressed list of existing rooms is updated.

![](images/roomlist.png)

